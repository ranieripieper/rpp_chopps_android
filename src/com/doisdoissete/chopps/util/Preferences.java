package com.doisdoissete.chopps.util;

import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.chopps.model.ChoppTypes;

public class Preferences {

	private static final String PREFERENCES = Preferences.class + "";
	private static final String PREF_REG_ID = "PREF_REG_ID";
	private static final String PREF_USER_TOKEN = "PREF_USER_TOKEN";
	private static final String PREF_CHOPP_TYPES = "PREF_CHOPP_TYPES";

	public static String getRegId(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		return settings.getString(PREF_REG_ID, null);
	}
	
	public static void setRegId(Context ctx, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_REG_ID, value);
		editor.commit();
	}
	
	public static String getUserToken(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		return settings.getString(PREF_USER_TOKEN, null);
	}
	
	public static void setUserToken(Context ctx, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_USER_TOKEN, value);
		editor.commit();
	}
	
	private static String getChoppTypesStr(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		return settings.getString(PREF_CHOPP_TYPES, null);
	}
	
	public static ChoppTypes[] getChoppTypes(Context ctx) {
		String json = getChoppTypesStr(ctx);
		return Util.getGson().fromJson(json, ChoppTypes[].class);
	}
	
	public static void setChoppTypes(Context ctx, ChoppTypes[] obj) {
		String value = ChoppTypes.toJson(obj);		 
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_CHOPP_TYPES, value);
		editor.commit();
	}
	
}
