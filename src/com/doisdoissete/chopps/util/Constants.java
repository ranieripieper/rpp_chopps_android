package com.doisdoissete.chopps.util;

import com.doisdoissete.chopps.model.Bar;
import com.doisdoissete.chopps.model.PackageBar;
import com.doisdoissete.chopps.model.User;

public class Constants {

	public static String TAG_DEBUG = "#dds_chopps";
	public static int ANIM_DRATION_HOME = 1500;
	//public static String SERVICE_HOST = "http://192.168.0.11";
	public static String SERVICE_HOST = "http://choppsdds.herokuapp.com";
	public static String LOGIN_PATH =  SERVICE_HOST + "/api/v1/sessions.json";
	public static String SIGN_UP_PATH =  SERVICE_HOST + "/api/v1/registrations.json";
	public static String GET_USER_INFO_PATH =  String.format("%s/api/v1/user.json?%s={%s}&%s={%s}&%s={%s}", SERVICE_HOST, User.FIELD_LAT, User.FIELD_LAT, User.FIELD_LNG, User.FIELD_LNG, User.FIELD_USER_TOKEN, User.FIELD_USER_TOKEN);
	
	public static String SEARCH_BAR_PATH = String.format("%s/api/v1/bars.json?%s={%s}&%s={%s}&%s={%s}", SERVICE_HOST, Bar.FIELD_LAT, Bar.FIELD_LAT, Bar.FIELD_LNG, Bar.FIELD_LNG, Bar.FIELD_SEARCH, Bar.FIELD_SEARCH);
	public static String SEARCH_BAR_WITHOUT_LOC_PATH = String.format("%s/api/v1/bars.json?%s={%s}", SERVICE_HOST, Bar.FIELD_SEARCH, Bar.FIELD_SEARCH);
	
	public static String GET_BAR_PATH = String.format("%s/api/v1/bar/{%s}.json", SERVICE_HOST, Bar.FIELD_ID);
	
	public static String GET_PACKAGES_NOT_CONSUMED = String.format("%s/api/v1/user/my_packages_not_consumed.json?%s={%s}&%s={%s}", SERVICE_HOST, Bar.FILTER_BAR_ID, Bar.FILTER_BAR_ID, User.FIELD_USER_TOKEN, User.FIELD_USER_TOKEN);
	
	public static String SEARCH_PACKAGE_PATH = String.format("%s/api/v1/packages/search.json?%s={%s}&%s={%s}", SERVICE_HOST, PackageBar.FIELD_VALUE, PackageBar.FIELD_VALUE, PackageBar.FIELD_TYPE, PackageBar.FIELD_TYPE);
	
	public static String SENDER_ID = "377298774511";
}
