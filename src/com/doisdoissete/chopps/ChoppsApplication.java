package com.doisdoissete.chopps;

import java.io.File;

import android.app.Application;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

public class ChoppsApplication extends Application {  

	public static ImageLoader imageLoader;
	
    @Override
    public void onCreate() {
        super.onCreate();
        
        DisplayImageOptions options = new DisplayImageOptions.Builder()
	        .resetViewBeforeLoading(false)  // default
	        .delayBeforeLoading(100)
	        .considerExifParams(true)
	        .cacheInMemory(true) // default
	        .cacheOnDisc(true).build(); // default

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(options) // default
                .writeDebugLogs()
                .build();
        
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }
}   
