package com.doisdoissete.chopps.view.activity;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.ExpandableHeightListView;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.Bar;
import com.doisdoissete.chopps.model.PackageBar;
import com.doisdoissete.chopps.model.SearchPackageResult;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.view.activity.base.BaseActivity;
import com.doisdoissete.chopps.view.adapter.PackageSearchAdapter;

public class SearchPackageActivity extends BaseActivity {

	private ImageView imgClaro;
	private ImageView imgEscuro;
	private ImageView imgSelect;
	private AsyncTaskService async;
	private Integer lastLeft;
	private final Integer DEFAULT_DURATION = 500;
	private SeekBar seekBar;
	private RelativeLayout layoutInfoSeek;
	private TextView txtProgress;
	private View viewSepSeekBar;
	private int widthSeek = 0;
	private final Integer DEFAULT_INIT_SEEK = 30;
	private Integer choopType = 1;
	private ExpandableHeightListView lstView;
	private PackageSearchAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_package_activity);
		
		imgClaro = (ImageView)findViewById(R.id.img_chopp_claro);
		imgEscuro = (ImageView)findViewById(R.id.img_chopp_escuro);
		imgSelect = (ImageView)findViewById(R.id.img_select);
		seekBar = (SeekBar)findViewById(R.id.seek_bar_preco);
		layoutInfoSeek = (RelativeLayout)findViewById(R.id.layout_info_seek);
		txtProgress = (TextView)findViewById(R.id.txt_preco);
		viewSepSeekBar = findViewById(R.id.view_sep);
		lstView = (ExpandableHeightListView)findViewById(R.id.list_view);
		
		lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Bar bar = adapter.getItem(position);
				ViewBarActivity.showActivity(SearchPackageActivity.this, bar);
				
			}
			
		});
		View v = findViewById(R.id.img_achar_bar_seak);
		v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		widthSeek = v.getMeasuredWidth();
	    
		imgClaro.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				selectChopp(imgClaro, DEFAULT_DURATION, 1);
			}
		});
		
		imgEscuro.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				selectChopp(imgEscuro, DEFAULT_DURATION, 2);
			}
		});
		
		imgClaro.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		
		imgClaro.post(new Runnable() {
			
			@Override
			public void run() {
				lastLeft = imgClaro.getLeft() - imgClaro.getMeasuredWidth()/2 - imgSelect.getWidth()/4;
				selectChopp(imgClaro, 0, 1);
			}
		});
		
		//SeekBar
		seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				callSearch();
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				
				setPositionSeekBarHint(progress);
			}
		});
		
		seekBar.post(new Runnable() {
			
			@Override
			public void run() {
				setPositionSeekBarHint(0);
			}
		});
		
		//callSearch();
		
	}
	
	private void setPositionSeekBarHint(int progress) {
		progress = progress + DEFAULT_INIT_SEEK;
        txtProgress.setText(getString(R.string.txt_preco, String.valueOf(progress)+",00"));
        layoutInfoSeek.setX(getXPosition(seekBar) + viewSepSeekBar.getWidth() - widthSeek/4);
	}
	
	private float getXPosition(SeekBar seekBar){
        float val = (((float)seekBar.getProgress() * (float)(seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax()) ;
        return val;
    }
	
	
	private synchronized void selectChopp(ImageView img, int duration, int type) {
		Integer nextLeft = img.getLeft() - img.getWidth()/2 - imgSelect.getWidth()/4;
		
		TranslateAnimation animation = new TranslateAnimation(lastLeft, nextLeft, imgSelect.getTop(), imgSelect.getTop());
		animation.setDuration(duration);
		animation.setFillAfter(true);

		choopType = type;
		imgSelect.startAnimation(animation);
		lastLeft = nextLeft;
		callSearch();
	}
	
	private synchronized void callSearch() {
		if (async != null) {
			async.cancel(true);
		}
		
		ObserverAsyncTask<SearchPackageResult> observer = new ObserverAsyncTask<SearchPackageResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);				
			}

			@Override
			public void onPostExecute(SearchPackageResult result) {
				dismissWaitDialog();
				if (result != null) {
					adapter = new PackageSearchAdapter(SearchPackageActivity.this, result.getBars(), getMapChoppTypes());
					lstView.setAdapter(adapter);
					lstView.setExpanded(true);
				}
				
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(PackageBar.FIELD_TYPE, String.valueOf(choopType));
		params.add(PackageBar.FIELD_VALUE, String.valueOf(seekBar.getProgress() + DEFAULT_INIT_SEEK));
		
		Location loc = GpsUtil.getLocation(SearchPackageActivity.this);
		
		String url = Constants.SEARCH_PACKAGE_PATH;
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(this, SearchPackageResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	@Override
	protected boolean isHideActionBar() {
		return false;
	}

}
