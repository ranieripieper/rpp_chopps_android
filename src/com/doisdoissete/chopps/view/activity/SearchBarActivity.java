package com.doisdoissete.chopps.view.activity;

import java.util.HashMap;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.FixedHeaderListView;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.Bar;
import com.doisdoissete.chopps.model.SearchBarResult;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.view.activity.base.BaseActivity;
import com.doisdoissete.chopps.view.adapter.MarkerAdapter;
import com.doisdoissete.chopps.view.adapter.SearchBarAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class SearchBarActivity extends BaseActivity {

	private FixedHeaderListView mListView;
	private GoogleMap map;
	private EditText edtSearchBar;
	private SearchBarAdapter adapter;
	private ImageView btSearchBar;
	private AsyncTaskService async;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_bar_activity);
		map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
		map.getUiSettings().setZoomControlsEnabled(false);
		map.setMyLocationEnabled(true);
		
		btSearchBar = (ImageView)findViewById(R.id.bt_search_bar);
		mListView = (FixedHeaderListView)findViewById(R.id.fixed_header_list_view);
		adapter = new SearchBarAdapter((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE), SearchBarActivity.this, null);
		mListView.setAdapter(adapter);
		edtSearchBar = (EditText)findViewById(R.id.edt_search_bar);

		edtSearchBar.setOnKeyListener(new OnKeyListener() {
		    public boolean onKey(View v, int keyCode, KeyEvent event) {
		        if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
		            (keyCode == KeyEvent.KEYCODE_ENTER)) {
		        	callSearch();
		          return true;
		        }
		        return false;
		    }
		});
		
		GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
		    @Override
		    public void onMyLocationChange(Location location) {
		        LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
		        if(map != null){
		        	map.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 12.0f));
		        	map.setOnMyLocationChangeListener(null);
		        }
		    }
		};
		
		callSearch();
		
		btSearchBar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				callSearch();
			}
		});
		
		map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
			
			@Override
			public void onInfoWindowClick(Marker marker) {
				Toast.makeText(SearchBarActivity.this, marker.getTitle(), Toast.LENGTH_SHORT).show();
			}
		});
		
		mListView.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				if (position >= 1) {
					Bar bar = adapter.getItem(position-1);
					
					ViewBarActivity.showActivity(SearchBarActivity.this, bar);
				}
			}
			
		});
		
	}


	private synchronized void callSearch() {
		InputMethodManager imm = 
			    (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edtSearchBar.getWindowToken(), 0);
		if (async != null) {
			async.cancel(true);
		}
		
		ObserverAsyncTask<SearchBarResult> observer = new ObserverAsyncTask<SearchBarResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);				
			}

			@Override
			public void onPostExecute(SearchBarResult result) {
				dismissWaitDialog();
				if (result != null && result.getBars() != null) {
					adapter.setBars(result.getBars());
					
					adapter.notifyDataSetChanged();
					mListView.requestLayout();
					map.clear();
					HashMap<Marker, Bar> markers = new HashMap<Marker, Bar>();
					
					LatLng loc = null; 
					for (Bar bar : result.getBars()) {
						if (loc == null) {
							loc = new LatLng(bar.getAddress().getCoordinates()[1], bar.getAddress().getCoordinates()[0]);
							map.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 14.0f));
						}
						Marker m = map.addMarker(new MarkerOptions()
							.icon(BitmapDescriptorFactory.fromResource(R.drawable.img_pin_bares))
							.position(new LatLng(bar.getAddress().getCoordinates()[1], bar.getAddress().getCoordinates()[0]))
							.title(bar.getName().toUpperCase())
							.snippet(bar.getAddress().getAddress()));
						
						markers.put(m, bar);
					}
					
					map.setInfoWindowAdapter(new MarkerAdapter((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE), SearchBarActivity.this, markers));
					
				}
				
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(Bar.FIELD_SEARCH, edtSearchBar.getText().toString());
		
		Location loc = GpsUtil.getLocation(SearchBarActivity.this);
		
		String url = Constants.SEARCH_BAR_PATH;
		if (loc != null) {
			params.add(Bar.FIELD_LAT, String.valueOf(loc.getLatitude()));
			params.add(Bar.FIELD_LNG, String.valueOf(loc.getLongitude()));
		} else {
			url = Constants.SEARCH_BAR_WITHOUT_LOC_PATH;
		}
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(this, SearchBarResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	protected boolean isHideActionBar() {
		return false;
	}
	
	@Override
	protected boolean customOpenClose() {
		return false;
	}
}
