package com.doisdoissete.chopps.view.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devsmart.android.ui.HorizontalListView;
import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.CircularImageView;
import com.doisdoissete.chopps.ChoppsApplication;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.Bar;
import com.doisdoissete.chopps.model.PackageBar;
import com.doisdoissete.chopps.model.ViewBarResult;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.view.activity.base.BaseActivity;
import com.doisdoissete.chopps.view.adapter.PackageBarAdapter;
import com.doisdoissete.chopps.view.custom.LockableScrollView;

public class ViewBarActivity extends BaseActivity {

	public static final String PARAM_BAR_ID = "PARAM_BAR_ID";
	public static final String PARAM_DISTANCE = "PARAM_DISTANCE";
	public static final String PARAM_BAR_NAME = "PARAM_BAR_NAME";
	public static final String PARAM_BAR_LOGO = "PARAM_BAR_LOGO";
	
	private CircularImageView imgLogo;
	private TextView txtBarName;
	private TextView txtDistance;
	private TextView txtPhone;
	private TextView txtAddress;
	private String barId;
	private Bar bar;
	private LinearLayout layoutPacotesDisponiveis;
	private Map<Integer, List<PackageBar>> mapQtdePackageBar = new HashMap<Integer, List<PackageBar>>();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_bar_activity);
		
		resizeHeaderSeparator();
		
		imgLogo = (CircularImageView)findViewById(R.id.logo_bar);
		txtBarName = (TextView)findViewById(R.id.txt_bar_name);
		txtDistance = (TextView)findViewById(R.id.txt_distance);
		txtPhone = (TextView)findViewById(R.id.txt_phone);
		txtAddress = (TextView)findViewById(R.id.txt_address);
		layoutPacotesDisponiveis = (LinearLayout)findViewById(R.id.layout_pacotes_disponiveis);
		
		prePopulate();
		
		callService();
	}
	
	private void prePopulate() {
		if (getIntent() != null && getIntent().getExtras() != null) {
			Bundle extras = getIntent().getExtras();
			if (extras.getString(PARAM_BAR_ID) == null) {
				finish();
			} else {
				barId = extras.getString(PARAM_BAR_ID);
			}
			
			if (extras.getString(PARAM_BAR_NAME) != null) {
				txtBarName.setText(extras.getString(PARAM_BAR_NAME));
			}
			
			if (extras.getString(PARAM_BAR_LOGO) != null) {
				ChoppsApplication.imageLoader.displayImage(extras.getString(PARAM_BAR_LOGO), imgLogo);
			}
			
			if (extras.getString(PARAM_DISTANCE) != null) {
				txtDistance.setText(getString(R.string.txt_km, extras.getString(PARAM_DISTANCE)));
			}
		}
	}
	
	private void populatePacotesDisponiveis() {
		if (bar != null && bar.getPackageBars() != null) {
			//agrupa packageBars por quantidade
			List<PackageBar> lstPackageBar = Arrays.asList(bar.getPackageBars());
			Collections.sort(lstPackageBar, new Comparator<PackageBar>() {
				@Override
				public int compare(PackageBar lhs, PackageBar rhs) {
					return Integer.compare(lhs.getQtde(), rhs.getQtde());
				}
			});
			
			for (PackageBar pckBar : lstPackageBar) {
				List<PackageBar> lstTmp = mapQtdePackageBar.get(pckBar.getQtde());
				if (lstTmp == null) {
					lstTmp = new ArrayList<PackageBar>();
				}
				lstTmp.add(pckBar);
				mapQtdePackageBar.put(pckBar.getQtde(), lstTmp);				
			}
			
			SortedSet<Integer> keys = new TreeSet<Integer>(mapQtdePackageBar.keySet());
			for (Integer key : keys) { 
			   List<PackageBar> value = mapQtdePackageBar.get(key);
			   createHorizontalListView(value);
			}
		}
	}
	
	private void createHorizontalListView( List<PackageBar> lstPackageBar) {
		HorizontalListView horizontalListView = new HorizontalListView(ViewBarActivity.this, null);
		horizontalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				findViewById(R.id.scroll_view).setEnabled(false);
				findViewById(R.id.scroll_view).setClickable(false);
			}
		});
		final LockableScrollView scrolView = (LockableScrollView)findViewById(R.id.scroll_view);
		horizontalListView.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				//scrolView.setScrollingEnabled(false);
				return false;
				
			}
		});

		PackageBarAdapter adapter = new PackageBarAdapter(ViewBarActivity.this, lstPackageBar, getMapChoppTypes());
		horizontalListView.setAdapter(adapter);
		
		layoutPacotesDisponiveis.addView(horizontalListView);
		
		PackageBarAdapter.setListViewSize(horizontalListView);
	}
	
	private void showContent() {
		
		if (bar != null) {
			txtAddress.setText(bar.getAddress().getAddressToView());
			txtPhone.setText(bar.getPhone());
			txtPhone.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Util.callTo(ViewBarActivity.this, bar.getPhone());
				}
			});
			txtAddress.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Util.openMap(ViewBarActivity.this, bar.getAddress().getCoordinates()[1], bar.getAddress().getCoordinates()[0]);
				}
			});
			populatePacotesDisponiveis();
		}
		
	}
	
	private void callService() {
		ObserverAsyncTask<ViewBarResult> observer = new ObserverAsyncTask<ViewBarResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);				
			}

			@Override
			public void onPostExecute(ViewBarResult result) {
				dismissWaitDialog();
				bar = result.getBar();
				showContent();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(Bar.FIELD_ID, barId);

		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Constants.GET_BAR_PATH)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(this, ViewBarResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void resizeHeaderSeparator() {
		View sep1 = findViewById(R.id.view_sep_1);
		View sep2 = findViewById(R.id.view_sep_2);
		
		int widthScreen = ScreenUtil.getDisplayWidth(ViewBarActivity.this);
		
		ViewGroup.LayoutParams params = sep1.getLayoutParams();
        params.width = widthScreen/2 - (widthScreen/6);
        sep1.setLayoutParams(params);
        
		params = sep2.getLayoutParams();
        params.width = widthScreen/2;
        sep2.setLayoutParams(params);
	}

	@Override
	protected boolean isHideActionBar() {
		return false;
	}
	
	public static void showActivity(Context mContext, Bar bar) {
		Intent it = new Intent(mContext, ViewBarActivity.class);
		it.putExtra(ViewBarActivity.PARAM_BAR_ID, bar.getId());
		it.putExtra(ViewBarActivity.PARAM_BAR_LOGO, bar.getLogo());
		it.putExtra(ViewBarActivity.PARAM_BAR_NAME, bar.getName());
		it.putExtra(ViewBarActivity.PARAM_DISTANCE, bar.getGeoNearDistanceStr());
		
		mContext.startActivity(it);
	}
}