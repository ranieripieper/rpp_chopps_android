package com.doisdoissete.chopps.view.activity;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.doisdoissete.android.util.ddsutil.ValidatorUtil;
import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.chopps.MainActivity;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.User;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.util.Preferences;
import com.doisdoissete.chopps.view.activity.base.BaseActivity;

public class LoginActivity extends BaseActivity {

	private EditText edtEmail;
	private EditText edtPassword;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.login_activity);
		
		edtEmail = (EditText)findViewById(R.id.txt_email);
		edtPassword = (EditText)findViewById(R.id.txt_password);
		
		edtEmail.addTextChangedListener(new TextWatcher(){
	        public void afterTextChanged(Editable s) {
	        	edtEmail.setError(null);
	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){}
	    }); 
		edtPassword.addTextChangedListener(new TextWatcher(){
	        public void afterTextChanged(Editable s) {
	        	edtPassword.setError(null);
	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){}
	    });
	}
	
	@Override
	protected boolean isHideActionBar() {
		return true;
	}

	public void loginClick(View v) {
		
		if (isValid()) {
			String regId = Preferences.getRegId(LoginActivity.this);
			if (regId == null) {
				callGcmService();
			} else {
				callLoginService();
			}
		}
	}
	
	private void callGcmService() {
		
		ObserverAsyncTask<String> observer = new ObserverAsyncTask<String>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(String result) {
				Intent intent = new Intent();                 
			    setResult(RESULT_OK, intent);                    
			    finish();
			    dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		callGcmRegistration(observer);
	}
	
	private void callLoginService() {
		ObserverAsyncTask<String> observer = new ObserverAsyncTask<String>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(String result) {
				dismissWaitDialog();
				Intent intent = new Intent();    
				intent.putExtra(MainActivity.INTENT_PARAM_HOME, result);
			    setResult(RESULT_OK, intent);                    
			    finish();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(User.FIELD_EMAIL, edtEmail.getText().toString());
		params.add(User.FIELD_PASSWORD, edtPassword.getText().toString());
		
		Location loc = GpsUtil.getLocation(LoginActivity.this);
		if (loc != null) {
			params.add(User.FIELD_LAT, String.valueOf(loc.getLatitude()));
			params.add(User.FIELD_LNG, String.valueOf(loc.getLongitude()));
		}
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Constants.LOGIN_PATH)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(this, String.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private boolean isValid() {
		boolean valid = true;
		if (TextUtils.isEmpty(edtEmail.getText()) || !ValidatorUtil.emailIsValid(edtEmail.getText().toString())) {
			setErrorMsg("", edtEmail);
			valid = false;
		}
		if (TextUtils.isEmpty(edtPassword.getText())) {
			setErrorMsg("", edtPassword);
			valid = false;
		}
		return valid;
	}
}
