package com.doisdoissete.chopps.view.activity;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.Bar;
import com.doisdoissete.chopps.model.PackageUserResult;
import com.doisdoissete.chopps.model.User;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.util.Preferences;
import com.doisdoissete.chopps.view.activity.base.BaseActivity;

public class DrinkChoppEnterPasswordActivity extends BaseActivity {

	public static final String PARAM_BAR_ID = "PARAM_BAR_ID";
	public static final String PARAM_PACKAGE_ID = "PARAM_PACKAGE_ID";
	
	private String barId = null;
	private String packageId = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drink_chopp_enter_password_activity);	
		
		if (getIntent() != null && getIntent().getExtras() != null && !TextUtils.isEmpty(getIntent().getExtras().getString(PARAM_BAR_ID))
				&& !TextUtils.isEmpty(getIntent().getExtras().getString(PARAM_PACKAGE_ID))) {
			barId = getIntent().getExtras().getString(PARAM_BAR_ID);
			packageId = getIntent().getExtras().getString(PARAM_PACKAGE_ID);
		} else {
			finish();
		}
		
		findViewById(R.id.bt_confirmar).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				findViewById(R.id.layout_enter_password).setVisibility(View.GONE);
				findViewById(R.id.layout_result).setVisibility(View.VISIBLE);
			}
		});
	}
	
	private void callService() {
		ObserverAsyncTask<PackageUserResult> observer = new ObserverAsyncTask<PackageUserResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);				
			}

			@Override
			public void onPostExecute(PackageUserResult result) {
				dismissWaitDialog();
				
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				e.printStackTrace();
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(Bar.FILTER_BAR_ID, barId);
		params.add(User.FIELD_USER_TOKEN, Preferences.getUserToken(DrinkChoppEnterPasswordActivity.this));

		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Constants.GET_PACKAGES_NOT_CONSUMED)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(this, PackageUserResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	protected boolean isHideActionBar() {
		return false;
	}
	
	public static void showActivity(Context mContext, String barId, String packageId) {
		Intent it = new Intent(mContext, DrinkChoppEnterPasswordActivity.class);
		it.putExtra(PARAM_BAR_ID, barId);
		it.putExtra(PARAM_PACKAGE_ID, packageId);
		mContext.startActivity(it);
	}
}
