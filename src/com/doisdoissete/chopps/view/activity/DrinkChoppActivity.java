package com.doisdoissete.chopps.view.activity;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.Bar;
import com.doisdoissete.chopps.model.PackageUserResult;
import com.doisdoissete.chopps.model.PackageUsers;
import com.doisdoissete.chopps.model.User;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.util.Preferences;
import com.doisdoissete.chopps.view.activity.base.BaseActivity;
import com.doisdoissete.chopps.view.adapter.ChoosePackageToDrinkAdapter;

public class DrinkChoppActivity extends BaseActivity {

	public static final String PARAM_BAR_ID = "PARAM_BAR_ID";
	
	private String barId = null;
	private PackageUsers[] packages;
	private ChoosePackageToDrinkAdapter adapter;
	private ListView listView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drink_chopp_activity);	
		
		if (getIntent() != null && getIntent().getExtras() != null && !TextUtils.isEmpty(getIntent().getExtras().getString(PARAM_BAR_ID))) {
			barId = getIntent().getExtras().getString(PARAM_BAR_ID);
			callService();
		} else {
			finish();
		}
		
		listView = (ListView)findViewById(R.id.list_view);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				PackageUsers pkgUser = adapter.getItem(position);
				DrinkChoppEnterPasswordActivity.showActivity(DrinkChoppActivity.this, barId, pkgUser.getId());
			}
		});
	}
	
	private void callService() {
		ObserverAsyncTask<PackageUserResult> observer = new ObserverAsyncTask<PackageUserResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);				
			}

			@Override
			public void onPostExecute(PackageUserResult result) {
				dismissWaitDialog();
				if (result != null && result.isSuccess() && result.getPackages() != null && 
						result.getPackages().length > 0) {
					packages = result.getPackages();
					adapter = new ChoosePackageToDrinkAdapter(DrinkChoppActivity.this, packages, getMapChoppTypes());
					listView.setAdapter(adapter);
				} else {
					finish();
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				e.printStackTrace();
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(Bar.FILTER_BAR_ID, barId);
		params.add(User.FIELD_USER_TOKEN, Preferences.getUserToken(DrinkChoppActivity.this));

		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Constants.GET_PACKAGES_NOT_CONSUMED)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(this, PackageUserResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	protected boolean isHideActionBar() {
		return false;
	}
	
	public static void showActivity(Context mContext, String barId) {
		Intent it = new Intent(mContext, DrinkChoppActivity.class);
		it.putExtra(PARAM_BAR_ID, barId);
		mContext.startActivity(it);
	}
}
