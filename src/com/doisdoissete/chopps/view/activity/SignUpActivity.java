package com.doisdoissete.chopps.view.activity;

import java.io.File;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.doisdoissete.android.util.ddsutil.ValidatorUtil;
import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.CircularImageView;
import com.doisdoissete.chopps.ChoppsApplication;
import com.doisdoissete.chopps.MainActivity;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.ExceptionResponse;
import com.doisdoissete.chopps.model.User;
import com.doisdoissete.chopps.service.RegisterUserService;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.view.activity.base.BaseActivity;

import es.javocsoft.android.lib.toolbox.ToolBox;
import es.javocsoft.android.lib.toolbox.media.MediaScannerNotifier;

public class SignUpActivity extends BaseActivity {

	private final int PICTURE_TAKEN_FROM_CAMERA = 1;
	private final int PICTURE_TAKEN_FROM_GALLERY = 2;
	
	private CircularImageView imgPhoto;
	private File outFile = null;
	private String filePath = "/storage/sdcard0/DCIM/Camera/20140901_093443.jpg";
	
	private boolean storeImage = true;
	private EditText edtName;
	private EditText edtEmail;
	private EditText edtPhone;
	private EditText edtPassword;
	private EditText edtPasswordConf;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.sign_up_activity);
		imgPhoto = (CircularImageView)findViewById(R.id.img_photo);
		
		edtName = (EditText)findViewById(R.id.txt_nome);
		edtEmail = (EditText)findViewById(R.id.txt_email);
		edtPhone = (EditText)findViewById(R.id.txt_telefone);
		edtPassword = (EditText)findViewById(R.id.txt_password);
		edtPasswordConf = (EditText)findViewById(R.id.txt_conf_password);
	}

	@Override
	protected boolean isHideActionBar() {
		return true;
	}
	
	public void loadImageClick(View v) {
		AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        builder.setMessage(R.string.dialog_escolher_foto)
               .setPositiveButton(R.string.dialog_txt_camera, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   getPictureFromCamera();
                   }
               })
               .setNegativeButton(R.string.dialog_txt_galeria, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   getPictureFromGallery();
                   }
               });
        builder.create().show();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    Bitmap takenPictureData = null;

	    switch(requestCode){

	        case PICTURE_TAKEN_FROM_CAMERA:             
	            if(resultCode==Activity.RESULT_OK) {
	                takenPictureData = handleResultFromCamera(data);
	            }               
	            break;
	        case PICTURE_TAKEN_FROM_GALLERY:                
	            if(resultCode==Activity.RESULT_OK) {
	                takenPictureData = handleResultFromChooser(data);                   
	            }
	            break;          
	    }
	    //And show the result in the image view.
	    if(takenPictureData!=null){
	    	imgPhoto.setImageBitmap(takenPictureData);
	    	
			imgPhoto.requestLayout();
			imgPhoto.refreshBitmapShader();
			imgPhoto.refreshDrawableState();
	    }       
	}
	
	//AUXILIAR
	private Bitmap handleResultFromChooser(Intent data){
	    Bitmap takenPictureData = null;

	    Uri photoUri = data.getData();
	    if (photoUri != null){
	        try {
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};
	            Cursor cursor = getContentResolver().query(photoUri, filePathColumn, null, null, null); 
	            cursor.moveToFirst();
	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            filePath = cursor.getString(columnIndex);
	            cursor.close();

	            takenPictureData = ToolBox.media_getBitmapFromFile(new File(filePath), 160);

	        } catch(Exception e) {
	            ToolBox.dialog_showToastAlert(this, "Error getting selected image.", false);
	        }
	    }
	    

	    return takenPictureData;
	}

	private Bitmap handleResultFromCamera(Intent data) {
	    Bitmap takenPictureData = null;

	    if(data != null) {
	        Bundle extras = data.getExtras();
	        if (extras != null && extras.get("data") != null) {
	            takenPictureData = (Bitmap) extras.get("data");
	        }
	    } else {
	        try{
	            takenPictureData = ToolBox.media_getBitmapFromFile(outFile, 160);
	            takenPictureData = ToolBox.media_correctImageOrientation(outFile.getAbsolutePath());
	        } catch(Exception e) {
	            ToolBox.dialog_showToastAlert(this, "Error getting saved taken picture.", false);
	        }
	        
	    }

	    if(storeImage) {
	        //We add the taken picture file to the gallery so user can see the image directly                   
	        new MediaScannerNotifier(this, outFile.getAbsolutePath(), "image/*", false);
	        filePath = outFile.getAbsolutePath();
	    }

	    return takenPictureData;
	}

	private void getPictureFromCamera() {
	    boolean cameraAvailable = ToolBox.device_isHardwareFeatureAvailable(this, PackageManager.FEATURE_CAMERA);
	    if(cameraAvailable && 
	        ToolBox.system_isIntentAvailable(this, MediaStore.ACTION_IMAGE_CAPTURE)) {

	        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

	        //We prepare the intent to store the taken picture
	        try {
	            File outputDir = ToolBox.storage_getExternalPublicFolder(Environment.DIRECTORY_PICTURES, getString(R.string.app_name), true);
	            outFile = ToolBox.storage_createUniqueFileName("cameraPic", ".jpg", outputDir);

	            takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outFile));             
	            storeImage = true;
	        } catch(Exception e) {
	            ToolBox.dialog_showToastAlert(this, "Error setting output destination.", false);
	        }

	        startActivityForResult(takePicIntent, PICTURE_TAKEN_FROM_CAMERA);
	    } else {
	        if (cameraAvailable) {
	            ToolBox.dialog_showToastAlert(this, "No application that can receive the intent camera.", false);
	        } else {
	            ToolBox.dialog_showToastAlert(this, "No camera present!!", false);
	        }
	    }
	}

	private void getPictureFromGallery() {
	    //This takes images directly from gallery
	    Intent gallerypickerIntent = new Intent(Intent.ACTION_PICK);
	    gallerypickerIntent.setType("image/*");
	    startActivityForResult(gallerypickerIntent, PICTURE_TAKEN_FROM_GALLERY); 
	}
	
	
	public void cadastrarClick(View v) {
		if (isValid()) {
			callCadastro();
		}
	}
	
	private void callCadastro() {
		ObserverAsyncTask<String> observer = new ObserverAsyncTask<String>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(String result) {
				dismissWaitDialog();
				Intent intent = new Intent();    
				intent.putExtra(MainActivity.INTENT_PARAM_HOME, result);
			    setResult(RESULT_OK, intent);                    
			    finish();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				ExceptionResponse exResp = showError(e);
				if (exResp != null && exResp.getInfo() != null) {
					if (exResp.getInfo().getName() != null && exResp.getInfo().getName().length > 0) {
						setErrorMsg("", edtName);
					}
					if (exResp.getInfo().getPassword() != null && exResp.getInfo().getPassword().length > 0) {
						setErrorMsg("", edtPassword);
					}
					if (exResp.getInfo().getEmail() != null && exResp.getInfo().getEmail().length > 0) {
						setErrorMsg("", edtEmail);
					}
					if (exResp.getInfo().getPasswordConfirmation() != null && exResp.getInfo().getPasswordConfirmation().length > 0) {
						setErrorMsg("", edtPasswordConf);
					}
					if (exResp.getInfo().getPhone() != null && exResp.getInfo().getPhone().length > 0) {
						setErrorMsg("", edtPhone);
					}
					
				}
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add(User.FIELD_NAME, edtName.getText().toString());
		params.add(User.FIELD_EMAIL, edtEmail.getText().toString());
		params.add(User.FIELD_PHONE, edtPhone.getText().toString());
		params.add(User.FIELD_PASSWORD, edtPassword.getText().toString());
		params.add(User.FIELD_PASSWORD_CONF, edtPasswordConf.getText().toString());
		if (outFile != null) {
			params.add(User.FIELD_PHOTO, new FileSystemResource(outFile));
		} else if (filePath != null) {
			params.add(User.FIELD_PHOTO, new FileSystemResource(filePath));
		}
		
		Location loc = GpsUtil.getLocation(SignUpActivity.this);
		if (loc != null) {
			params.add(User.FIELD_LAT, String.valueOf(loc.getLatitude()));
			params.add(User.FIELD_LNG, String.valueOf(loc.getLongitude()));
		}
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Constants.SIGN_UP_PATH)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.addHeaders("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(this, String.class, new RegisterUserService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private boolean isValid() {
		boolean valid = true;
		if (TextUtils.isEmpty(edtName.getText())) {
			setErrorMsg("", edtName);
			valid = false;
		}
		if (TextUtils.isEmpty(edtPhone.getText())) {
			setErrorMsg("", edtPhone);
			valid = false;
		}
		if (TextUtils.isEmpty(edtPassword.getText())) {
			setErrorMsg("", edtPassword);
			valid = false;
		}
		if (TextUtils.isEmpty(edtPasswordConf.getText())) {
			setErrorMsg("", edtPasswordConf);
			valid = false;
		}
		if (TextUtils.isEmpty(edtEmail.getText()) || !ValidatorUtil.emailIsValid(edtEmail.getText().toString())) {
			setErrorMsg("", edtEmail);
			valid = false;
		}
		if (edtPassword.getText() != null && edtPasswordConf.getText() != null && !edtPassword.getText().toString().equals(edtPasswordConf.getText().toString())) {
			setErrorMsg("", edtPassword);
			setErrorMsg("", edtPasswordConf);
			valid = false;
		}
		return valid;
	}
	
}
