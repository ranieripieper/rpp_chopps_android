package com.doisdoissete.chopps.view.activity.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;

import android.app.ProgressDialog;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.smoothprogressbar.SmoothProgressBar;
import com.doisdoissete.android.util.ddsutil.view.custom.smoothprogressbar.SmoothProgressDrawable;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.ChoppTypes;
import com.doisdoissete.chopps.model.ExceptionResponse;
import com.doisdoissete.chopps.service.gcm.GcmGetRegIdService;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.util.Preferences;
import com.doisdoissete.chopps.view.activity.ViewBarActivity;

public abstract class BaseActivity extends ActionBarActivity {

	private static Boolean DEBUG = null;
	static public boolean isAPI11 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	private ProgressDialog progress;
	protected SmoothProgressBar smoothProgressBar;
	protected TouchBlackHoleView black_hole;
	private static Map<Integer, ChoppTypes> mapChoppTypes = new HashMap<Integer, ChoppTypes>();
	
	protected Map<Integer, ChoppTypes> getMapChoppTypes() {
		if (mapChoppTypes.isEmpty()) {
			for (ChoppTypes choppType : Preferences.getChoppTypes(BaseActivity.this)) {
				mapChoppTypes.put(choppType.getId(), choppType);
			}
		}
		return mapChoppTypes;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (DEBUG == null) {
			DEBUG = ( 0 != ( getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE ) );
		}
		
		if (customOpenClose()) {
			getWindow().getAttributes().windowAnimations = R.style.OpenCloseActivity;
		}
		
		if (isHideActionBar()) {
			getSupportActionBar().hide();
		} else {
			getSupportActionBar().setCustomView(R.layout.custom_action_bar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowCustomEnabled(true);
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			getSupportActionBar().setDisplayUseLogoEnabled(false);
			getSupportActionBar().setLogo(null);
			getSupportActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
			
			int upId = Resources.getSystem().getIdentifier("up", "id", "android");
			if (upId > 0) {
			    final ImageView up = (ImageView) findViewById(upId);
			    up.setImageResource(R.drawable.ic_seta);
			    
			    up.post(new Runnable() {
					
					@Override
					public void run() {
						View v = getSupportActionBar().getCustomView().findViewById(R.id.header_logo);
						android.app.ActionBar.LayoutParams lp = (android.app.ActionBar.LayoutParams)v.getLayoutParams();
						lp.rightMargin = up.getWidth();
						v.setLayoutParams(lp);
					}
				});
			}
			
			View v = getSupportActionBar().getCustomView();
			LayoutParams lp = v.getLayoutParams();
			lp.width = LayoutParams.MATCH_PARENT;
			v.setLayoutParams(lp);
			
		}
        
	}
	
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		
		smoothProgressBar = (SmoothProgressBar)findViewById(R.id.progress_bar);
        
		if (smoothProgressBar != null) {
			smoothProgressBar.setIndeterminateDrawable(new SmoothProgressDrawable.Builder(BaseActivity.this)
        	.colors(getResources().getIntArray(R.array.progress_bar_color))
	        .interpolator(new DecelerateInterpolator())
	        .sectionsCount(1)
	        .separatorLength(0)         //You should use Resources#getDimensionPixelSize
	        .strokeWidth(10f)            //You should use Resources#getDimension
	        .speed(2f)                 //2 times faster
	        .progressiveStartSpeed(2)
	        .progressiveStopSpeed(3.4f)
	        .reversed(false)
	        .mirrorMode(false)
	        .progressiveStart(true)
	        .gradients(true)
	        .generateBackgroundUsingColors()
	        .build());
		}
		
		black_hole = (TouchBlackHoleView)findViewById(R.id.black_hole);
	}
	
	protected boolean customOpenClose() {
		return true;
	}
	
	protected void callGcmRegistration(ObserverAsyncTask<String> observer) {

			ServiceBuilder sb = new ServiceBuilder();
			sb.setObserverAsyncTask(observer)
				.setNeedConnection(true);
		
			AsyncTaskService async = sb.mappingInto(this, String.class, new GcmGetRegIdService(sb));
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				async.execute();
			}
		
	}
	
	/**
	 * M�todo para logar
	 * @param msg
	 */
	protected void log(String msg) {
		if (DEBUG) {
			Log.d(Constants.TAG_DEBUG, msg);
		}
	}
	
	protected void showMessage(String msg) {
		Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_SHORT).show();
	}
	
	protected void showMessage(int msg) {
		showMessage(getString(msg));
	}
	
	protected abstract boolean isHideActionBar();
	
	protected void showWaitDialog(boolean blockScreen) {
		if (black_hole != null) {
			if (blockScreen) {
				black_hole.disable_touch(true);
				black_hole.setVisibility(View.VISIBLE);
			} else {
				black_hole.disable_touch(false);
				black_hole.setVisibility(View.GONE);
			}
			
		}
		
		if (smoothProgressBar != null) {
			smoothProgressBar.setVisibility(View.VISIBLE);
		} else {
			progress = ProgressDialog.show(this, getString(R.string.title_aguarde),
					getString(R.string.msg_aguarde), true);
			progress.show();
		}
	}
	protected void showWaitDialog() {
		showWaitDialog(true);
	}
	
	protected void dismissWaitDialog() {
		if (black_hole != null) {
			black_hole.disable_touch(false);
			black_hole.setVisibility(View.GONE);
		}
		if (smoothProgressBar != null) {
			smoothProgressBar.setVisibility(View.GONE);
		}
		if (progress != null) {
			progress.dismiss();
		}
	}
	
	
	protected ExceptionResponse showError(Exception e) {
		dismissWaitDialog();
		
		ExceptionResponse result = null;
		String msgError = getString(R.string.msg_erro_conexao);
		
		if (e instanceof ConnectionException) {
			ConnectionException ex = (ConnectionException)e;
			
			if (HttpStatus.UNPROCESSABLE_ENTITY.equals(ex.getHttpStatus())) {
				
				ExceptionResponse exResp = Util.getGson().fromJson(ex.getMsgErro(), ExceptionResponse.class);
				
				msgError = exResp.toString();
				
				result = exResp;
			}
		}

		Toast.makeText(BaseActivity.this, msgError, Toast.LENGTH_SHORT).show();
		
		return result;
	}
	
	public void setErrorMsg(int msg, EditText viewId) {
		setErrorMsg(getString(msg), viewId);
	}
	
	public void setErrorMsg(String msg, EditText viewId) {
	    //Osama ibrahim 10/5/2013
/*	    int ecolor = Color.WHITE; // whatever color you want
	    String estring = msg;
	    ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
	    SpannableStringBuilder ssbuilder = new SpannableStringBuilder(estring);
	    ssbuilder.setSpan(fgcspan, 0, estring.length(), 0);*/
	    viewId.setError(msg);
	   // viewId.setl
	    //viewId.setError(Html.fromHtml("<div style='background-color:red;'><font color='yellow'>this is the error</font></div>"));

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	       finish();
	       return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
}
