package com.doisdoissete.chopps.view.adapter;

import java.util.Map;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devsmart.android.ui.HorizontalListView;
import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.chopps.ChoppsApplication;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.Bar;
import com.doisdoissete.chopps.model.ChoppTypes;
import com.doisdoissete.chopps.model.PackageBar;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.view.activity.SearchBarActivity;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderBarPackage;

public class PackageSearchAdapter extends BaseAdapter {

	private Context mContext;
	private Bar[] bars;
	private Map<Integer, ChoppTypes> choppTypes;
	
	public PackageSearchAdapter(Context context, Bar[] bars, Map<Integer, ChoppTypes> choppTypes) {
		this.mContext = context;
		this.bars = bars;
		this.choppTypes = choppTypes;
	}
	
	public static void setListViewSize(HorizontalListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            //do nothing return null
            return;
        }
        //set listAdapter in loop for getting final size
        int totalHeight = 0;
        if (myListAdapter.getCount() > 0) {
        	View listItem = myListAdapter.getView(0, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //setting listview item in adapter
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight;// + (myListView.getDividerHeight() * (myListAdapter.getCount() - 1));
        myListView.setLayoutParams(params);
    }

	@Override
	public int getCount() {
		if (bars == null) {
			return 0;
		}
		return bars.length;
	}

	@Override
	public Bar getItem(int position) {
		return bars[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		
		ViewHolderBarPackage holder = null;

		if (vi == null) {
			vi = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_package_row, parent, false);
			holder = new ViewHolderBarPackage();
			holder.imgBar = (ImageView)vi.findViewById(R.id.img_bar);
			holder.txtBar = (TextView)vi.findViewById(R.id.txt_bar_name);
			holder.txtKm = (TextView) vi.findViewById(R.id.txt_bar_km);
			
			holder.layoutPacote1 = (RelativeLayout) vi.findViewById(R.id.layout_pacote_1);
			holder.imgPacote1 = (ImageView) vi.findViewById(R.id.img_claro);
			holder.txtDescPacote1 = (TextView) vi.findViewById(R.id.txt_desc_pacote_1);
			holder.txtPrecoPacote1 = (TextView) vi.findViewById(R.id.txt_valor_pacote_1);
			
			holder.layoutPacote2 = (RelativeLayout) vi.findViewById(R.id.layout_pacote_2);
			holder.imgPacote2 = (ImageView) vi.findViewById(R.id.img_escuro);
			holder.txtDescPacote2 = (TextView) vi.findViewById(R.id.txt_desc_pacote_2);
			holder.txtPrecoPacote2 = (TextView) vi.findViewById(R.id.txt_valor_pacote_2);
			
			vi.setTag(holder);
		} else {
			holder = (ViewHolderBarPackage) vi.getTag();
		}

		Bar bar = bars[position];
		holder.txtBar.setText(bar.getName().toUpperCase());
		holder.txtKm.setText(mContext.getString(R.string.txt_km, GpsUtil.distance(mContext, bar.getAddress().getCoordinates()[1], bar.getAddress().getCoordinates()[0])));
		int i = 0;
		int maxLoop = Math.min(2, bar.getPackageBars().length);
		for (i=0; i < maxLoop; i++) {
			PackageBar pkgBar = bar.getPackageBars()[i];
			
			if (i ==0) {
				holder.txtDescPacote1.setText(mContext.getString(R.string.txt_qtde_type, pkgBar.getQtde(), pkgBar.getMark()));
				holder.txtPrecoPacote1.setText(mContext.getString(R.string.txt_preco, String.valueOf(pkgBar.getValue())));
			} else {
				holder.txtDescPacote2.setText(mContext.getString(R.string.txt_qtde_type, pkgBar.getQtde(), pkgBar.getMark()));
				holder.txtPrecoPacote2.setText(mContext.getString(R.string.txt_preco, String.valueOf(pkgBar.getValue())));
			}
			
		}
		if (i == 1 ) {
			holder.layoutPacote2.setVisibility(View.GONE);
		} else {
			holder.layoutPacote2.setVisibility(View.VISIBLE);
		}
		ChoppsApplication.imageLoader.displayImage(bar.getLogo(), holder.imgBar);
		
		return vi;
	}
	
}
