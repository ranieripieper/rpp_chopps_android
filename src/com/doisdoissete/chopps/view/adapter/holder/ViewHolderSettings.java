package com.doisdoissete.chopps.view.adapter.holder;

import android.view.View;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;

public class ViewHolderSettings {

	public TextViewPlus txtConfiguracao;
	public ImageView btnSettings;
	public View viewSep;
}
