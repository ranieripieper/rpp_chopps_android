package com.doisdoissete.chopps.view.adapter.holder;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ViewHolderBarPackages {

	public View viewSepTop;
	public LinearLayout layoutPreco;
	public View viewSepBottom;
	public Button btComprarPacote;
	public TextView txtMark;
	public TextView txtQtdeType;
	public TextView txtValor1;
	public TextView txtValor2;
	public ImageView imgChopps;
}
