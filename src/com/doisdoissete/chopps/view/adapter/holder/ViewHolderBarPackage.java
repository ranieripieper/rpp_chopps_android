package com.doisdoissete.chopps.view.adapter.holder;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ViewHolderBarPackage {
	public TextView txtBar;
	public TextView txtKm;
	public ImageView imgBar;
	public TextView txtDescPacote1;
	public TextView txtPrecoPacote1;
	public ImageView imgPacote1;
	public TextView txtDescPacote2;
	public TextView txtPrecoPacote2;
	public ImageView imgPacote2;
	public RelativeLayout layoutPacote1;
	public RelativeLayout layoutPacote2;
}
