package com.doisdoissete.chopps.view.adapter.holder;

import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.view.custom.ProgressWheel;

public class ViewHolderMyPackages {

	public ProgressWheel progressWheel;
	public TextView txtBarName;
	public TextView txtQtdeChopp;
	public TextView txtDtVencimento;
}
