package com.doisdoissete.chopps.view.adapter.holder;

import android.view.View;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.view.custom.CircularImageView;

public class ViewHolderProfile {
	public CircularImageView imgProfile;
	public TextView txtName;
	public TextView txtLastChoppDt;
	public TextView txtLastChoppBar;
	public View viewSep;
}
