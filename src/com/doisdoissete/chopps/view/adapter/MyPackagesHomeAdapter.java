package com.doisdoissete.chopps.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.devsmart.android.ui.HorizontalListView;
import com.doisdoissete.android.util.ddsutil.view.custom.ProgressWheel;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.PackageUsers;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderMyPackages;


public class MyPackagesHomeAdapter extends BaseAdapter {

	private Context mContext;
	private PackageUsers[] pkgUsers;
	
	public MyPackagesHomeAdapter(Context context, View parentView, PackageUsers[] pkgUsers) {
		this.mContext = context;
		this.pkgUsers = pkgUsers;
	}
	
	public static void setListViewSize(HorizontalListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            //do nothing return null
            return;
        }
        //set listAdapter in loop for getting final size
        int totalHeight = 0;
        if (myListAdapter.getCount() > 0) {
        	View listItem = myListAdapter.getView(0, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //setting listview item in adapter
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight;// + (myListView.getDividerHeight() * (myListAdapter.getCount() - 1));
        myListView.setLayoutParams(params);
    }

	@Override
	public int getCount() {
		if (pkgUsers == null) {
			return 0;
		}
		return pkgUsers.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView; // trying to reuse a recycled view

		ViewHolderMyPackages holder = null;

		if (vi == null) {
			vi = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_row_my_packges_row_view, parent, false);
			holder = new ViewHolderMyPackages();
			holder.progressWheel = (ProgressWheel)vi.findViewById(R.id.progress_wheel);
			holder.txtBarName = (TextView)vi.findViewById(R.id.txt_bar_name);
			holder.txtQtdeChopp = (TextView)vi.findViewById(R.id.txt_qtde_chopp);
			holder.txtDtVencimento = (TextView)vi.findViewById(R.id.txt_dt_vencimento);
			
			vi.setTag(holder);
		} else {
			holder = (ViewHolderMyPackages) vi.getTag();
			holder.progressWheel.stopAnimation();
		}

		PackageUsers pkgUser = pkgUsers[position];
		holder.txtBarName.setText(pkgUser.getBar().getName());
		holder.txtDtVencimento.setText(pkgUser.getDtExprireStr());
		holder.txtQtdeChopp.setText(mContext.getString(R.string.txt_qtde_chopps, pkgUser.getPackageBars().getQtde()));
		
		int total = pkgUser.getPackageBars().getQtde();
		int cons = pkgUser.getQtdeCons();

		int perc = cons * 100 / total;
		holder.progressWheel.startAnimation(perc, mContext.getString(R.string.txt_percentual_consumido));
		
		return vi;
	}
	
}
