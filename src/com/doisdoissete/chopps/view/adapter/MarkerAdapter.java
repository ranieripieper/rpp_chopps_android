package com.doisdoissete.chopps.view.adapter;

import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.Bar;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

public class MarkerAdapter  implements InfoWindowAdapter{
	private LayoutInflater mInflater;
	private HashMap<Marker, Bar> bars;
	private Context mContext;

	public MarkerAdapter(LayoutInflater i, Context context, HashMap<Marker, Bar> bars){
	    mInflater = i;
	    this.bars = bars;
	    this.mContext = context;
	}

	@Override
	public View getInfoContents(Marker marker) {
	    final View vi = mInflater.inflate(R.layout.search_bar_marker, null);

	    final TextView txtAddress = (TextView)vi.findViewById(R.id.txt_address);
	    final TextView txtBar = (TextView)vi.findViewById(R.id.txt_bar_name);
	    final TextView txtKm =(TextView) vi.findViewById(R.id.txt_bar_km);
		
		Bar bar = bars.get(marker);
		txtBar.setText(bar.getName().toUpperCase());
		txtKm.setText(mContext.getString(R.string.txt_km, bar.getGeoNearDistanceStr()));
		txtAddress.setText(bar.getAddress().getAddress());
		
	    return vi;
	}

	@Override
	public View getInfoWindow(Marker marker) {
	    // TODO Auto-generated method stub
	    return null;
	}
	}

