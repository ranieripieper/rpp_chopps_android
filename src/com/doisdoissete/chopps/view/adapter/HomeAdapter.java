package com.doisdoissete.chopps.view.adapter;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.devsmart.android.ui.HorizontalListView;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.CircularImageView;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.flipdigit.FlipDigitGroupView;
import com.doisdoissete.chopps.ChoppsApplication;
import com.doisdoissete.chopps.MainActivity;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.LoginResult;
import com.doisdoissete.chopps.view.activity.DrinkChoppActivity;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderBarLoc;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderButtons;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderDefault;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderHomeMyPackages;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderNrBars;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderProfile;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderSettings;

public class HomeAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private boolean showHome;
	private Context mContext;
	private boolean isScrolled = false;
	private LoginResult loginResult;
	
	private enum HomeTypesView {
		LOCATION(0), NR_BARS_VIEW(1), BUTTONS_VIEW(2), PROFILE_VIEW(3), PACKAGES_VIEW(4), HISTORY_VIEW(5), SETTINGS_VIEW(6), FOOTER(7);
		
	    private final int value;

	    private static final Map<Integer, HomeTypesView> typesByValue = new HashMap<Integer, HomeTypesView>();

	    static {
	        for (HomeTypesView type : HomeTypesView.values()) {
	            typesByValue.put(type.value, type);
	        }
	    }
	    
	    private HomeTypesView(int value) {
	        this.value = value;
	    }

	    @SuppressWarnings("unused")
		public static HomeTypesView forValue(int value) {
	        return typesByValue.get(value);
	    }
	}
	
	public HomeAdapter(LayoutInflater inflater, Context context, boolean showHome) {
		this(inflater, context, showHome, null);
	}
	
	public HomeAdapter(LayoutInflater inflater, Context context, boolean showHome, LoginResult loginResult) {
		this.inflater = inflater;
		this.showHome = showHome;
		this.mContext = context;
		this.loginResult = loginResult;
	}

	@Override
	public int getCount() {
		if (!showHome) {
			return 1;
		} else {
			return 8;
		}
	}
	
	@Override
	public int getViewTypeCount() {
		if (!showHome) {
			return 1;
		} else {
			return 8;
		}
	}

	@Override
	public int getItemViewType(int position) {

		if (!showHome) {
			return 1;
		} else {
			if (position == 0) {
				return HomeTypesView.LOCATION.value;
			} else if (position == 1) {
				return HomeTypesView.NR_BARS_VIEW.value;
			} else if (position == 2) {
				return HomeTypesView.BUTTONS_VIEW.value;
			} else if (position == 3) {
				return HomeTypesView.PROFILE_VIEW.value;
			} else if (position == 4) {
				return HomeTypesView.PACKAGES_VIEW.value;
			} else if (position == 5) {
				return HomeTypesView.HISTORY_VIEW.value;
			} else if (position == 6) {
				return HomeTypesView.SETTINGS_VIEW.value;
			} else if (position == 7) {
				return HomeTypesView.FOOTER.value;
			}
		}

		return 0;
	}
	
	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (!showHome) {
			return getViewSignIn(position, convertView, parent);
		} else {
			return getViewHome(position, convertView, parent);
		}
	}
	
	private View getViewHome(int position, View convertView, ViewGroup parent) {
		int type = getItemViewType(position);
		if (type == HomeTypesView.LOCATION.value) {
			return getViewHomeLocation(position, convertView, parent);
		} else if (type == HomeTypesView.NR_BARS_VIEW.value) {
			return getViewHomeNrBars(position, convertView, parent);
		} else if (type == HomeTypesView.BUTTONS_VIEW.value) {
			return getViewHomeButtons(position, convertView, parent);
		} else if (type == HomeTypesView.PROFILE_VIEW.value) {
			return getViewHomeProfile(position, convertView, parent);
		} else if (type == HomeTypesView.PACKAGES_VIEW.value) {
			return getViewHomePackages(position, convertView, parent);
		} else if (type == HomeTypesView.HISTORY_VIEW.value) {
			return getViewHomeHistory(position, convertView, parent);
		} else if (type == HomeTypesView.SETTINGS_VIEW.value) {
			return getViewHomeSettings(position, convertView, parent);
		} else if (type == HomeTypesView.FOOTER.value) {
			return getViewHomeFooter(position, convertView, parent);
		}
		return null;
	}
	
	private View getViewHomeLocation(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		ViewHolderBarLoc holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.home_row_location, parent, false);
			holder = new ViewHolderBarLoc();
			holder.barName = (TextView)vi.findViewById(R.id.txt_bar);
			holder.btDrinkChopp = (Button)vi.findViewById(R.id.bt_tomar_chopp_aqui);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderBarLoc ) vi.getTag();
		}
		
		if (loginResult.getBar() != null && loginResult.isUserHasPkg()) {
			holder.barName.setText(loginResult.getBar().getName().toUpperCase());
			holder.barName.setVisibility(View.VISIBLE);
			holder.btDrinkChopp.setText(R.string.bt_tomar_chopp_aqui);
			holder.btDrinkChopp.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					DrinkChoppActivity.showActivity(mContext, loginResult.getBar().getId());
				}
			});
		} else {
			holder.barName.setVisibility(View.GONE);
			holder.btDrinkChopp.setText(R.string.bt_tomar_chopp);
			holder.btDrinkChopp.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Toast.makeText(mContext, "TODO", Toast.LENGTH_SHORT).show();
				}
			});
		}

		return vi;
	}
	
	private View getViewHomeSettings(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		ViewHolderSettings holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.home_row_settings_view, parent, false);
			holder = new ViewHolderSettings();
			holder.btnSettings = (ImageView)vi.findViewById(R.id.btn_settings);
			holder.txtConfiguracao = (TextViewPlus)vi.findViewById(R.id.txt_settings);
			holder.viewSep = vi.findViewById(R.id.view_sep);
			
			//resize view_sep
			vi.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
			holder.txtConfiguracao.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
			
			View layoutTxtConfig = vi.findViewById(R.id.layout_txt_config);
			layoutTxtConfig.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
			
			holder.viewSep.getLayoutParams().height = vi.getMeasuredHeight()/2 - holder.txtConfiguracao.getMeasuredHeight();
			holder.viewSep.getLayoutParams().width = layoutTxtConfig.getMeasuredWidth()/2;
			((android.widget.RelativeLayout.LayoutParams)holder.viewSep.getLayoutParams()).topMargin = vi.getMeasuredHeight()/2;
			
			vi.setTag(holder);
		} else {
			holder = (ViewHolderSettings) vi.getTag();
		}
		
		return vi;
	}
	
	
	private View getViewHomeFooter(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		ViewHolderDefault holder = null;

		if (vi == null) {
			
			vi = inflater.inflate(R.layout.home_row_footer_view, parent, false);
			holder = new ViewHolderDefault();
			
			vi.setTag(holder);
		} else {
			holder = (ViewHolderDefault) vi.getTag();
		}

		return vi;
	}
	
	private View getViewHomeHistory(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		ViewHolderButtons holder = null;

		if (vi == null) {
			
			vi = inflater.inflate(R.layout.home_row_history_view, parent, false);
			holder = new ViewHolderButtons();
			
			vi.setTag(holder);
		} else {
			holder = (ViewHolderButtons) vi.getTag();
		}

		return vi;
	}
	
	private View getViewHomeNrBars(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		ViewHolderNrBars holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.home_row_nr_bars_view, parent, false);
			holder = new ViewHolderNrBars();
			holder.flipGroupView = (FlipDigitGroupView)vi.findViewById(R.id.flipViewGroup);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderNrBars) vi.getTag();
		}
		
		if (isScrolled) {
			/*int velocity = -1;
			if (loginResult.getNrBars() < 15) {
				velocity = 1000;
			} else if (loginResult.getNrBars() < 30) {
				velocity = 1200;
			} else if (loginResult.getNrBars() < 30) {
				velocity = 1200;
			}
			if (velocity > 0) {
				holder.flipGroupView.setVelocity(velocity);
			}*/
			
			holder.flipGroupView.setValue(loginResult.getNrBars());
		} else {
			holder.flipGroupView.scrollToDigit(loginResult.getNrBars());
			isScrolled = true;
		}
		
		
		return vi;
	}
	
	private View getViewHomePackages(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		ViewHolderHomeMyPackages holder = null;
		
		if (vi == null) {
			vi = inflater.inflate(R.layout.home_row_my_packages_view, parent, false);
			holder = new ViewHolderHomeMyPackages();

			holder.horizontalListView = (HorizontalListView)vi.findViewById(R.id.horizontal_listview);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderHomeMyPackages) vi.getTag();
		}

		if (loginResult.getUserModel() != null && loginResult.getUserModel().getPackageUsers() != null) {
			holder.horizontalListView.setAdapter(new MyPackagesHomeAdapter(mContext, vi, loginResult.getUserModel().getPackageUsers()));
		}
		
		MyPackagesHomeAdapter.setListViewSize(holder.horizontalListView);
		return vi;
	}

	
	private View getViewHomeButtons(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		ViewHolderButtons holder = null;

		if (vi == null) {
			
			vi = inflater.inflate(R.layout.home_row_buttons_view, parent, false);
			holder = new ViewHolderButtons();
			
			vi.setTag(holder);
		} else {
			// View recycled !
			// no need to inflate
			// no need to findViews by id
			holder = (ViewHolderButtons) vi.getTag();
		}

		return vi;
	}
	
	private View getViewHomeProfile(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		ViewHolderProfile holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.home_row_profile_view, parent, false);
			holder = new ViewHolderProfile();
			holder.imgProfile = (CircularImageView)vi.findViewById(R.id.img_photo);
			holder.txtName = (TextView)vi.findViewById(R.id.txt_profile_name);
			holder.txtLastChoppDt = (TextView)vi.findViewById(R.id.txt_data);
			holder.txtLastChoppBar = (TextView)vi.findViewById(R.id.txt_nome_bar);
			holder.viewSep = vi.findViewById(R.id.view_line_sep);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderProfile) vi.getTag();
		}

		//ajusta tamnaho da view de separacao
		LayoutParams lp = holder.viewSep.getLayoutParams();
		lp.width = ScreenUtil.getDisplayWidth(mContext)/2 - 20;
		holder.viewSep.setLayoutParams(lp);
		holder.txtName.bringToFront();
		
		String name = loginResult.getUserModel().getName();
		String[] nameSplit = name.split(" ");
		if (nameSplit.length >= 2) {
			name = nameSplit[0] + "\n" + nameSplit[1];
		}
		holder.txtName.setText(name);
		holder.imgProfile.setImageResource(R.drawable.img_white);

		if (loginResult.getUserModel() != null && !TextUtils.isEmpty(loginResult.getUserModel().getPhoto())) {
			ChoppsApplication.imageLoader.displayImage(loginResult.getUserModel().getPhoto(), holder.imgProfile);
		}
		holder.txtLastChoppBar.setText(loginResult.getUserModel().getLastChoppBar());
		holder.txtLastChoppDt.setText(loginResult.getUserModel().getLastChoppDtStr());
		return vi;
	}

	
	/**
	 * Retorna a View para usu�rio n�o logado
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return
	 */
	private View getViewSignIn(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		
		
		ViewHolderSignIn holder = null;

		if (vi == null) {			
			vi = inflater.inflate(R.layout.login_sing_up_view, parent, false);
			holder = new ViewHolderSignIn();
			vi.setTag(holder);
		} else {
			holder = (ViewHolderSignIn) vi.getTag();
		}

		return vi;
	}
	
	static class ViewHolderSignIn {
	}
}
