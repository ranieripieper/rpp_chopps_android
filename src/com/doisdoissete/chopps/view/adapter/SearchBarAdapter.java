package com.doisdoissete.chopps.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.chopps.ChoppsApplication;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.Bar;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderBar;

public class SearchBarAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private Context mContext;
	private Bar[] bars;
	
	public SearchBarAdapter(LayoutInflater inflater, Context context, Bar[] bars) {
		this.inflater = inflater;
		this.mContext = context;
		this.bars = bars;
	}

	@Override
	public int getCount() {
		if (bars == null) {
			return 0;
		}
		return bars.length;
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public Bar getItem(int position) {
		return bars[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView; // trying to reuse a recycled view
		
		ViewHolderBar holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.search_bar_row_bar, parent, false);
			holder = new ViewHolderBar();
			holder.imgBar = (ImageView)vi.findViewById(R.id.img_bar);
			holder.txtBar = (TextView)vi.findViewById(R.id.txt_bar_name);
			holder.txtKm = (TextView) vi.findViewById(R.id.txt_bar_km);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderBar) vi.getTag();
		}

		Bar bar = bars[position];
		holder.txtBar.setText(bar.getName().toUpperCase());
		holder.txtKm.setText(mContext.getString(R.string.txt_km, bar.getGeoNearDistanceStr()));
		
		ChoppsApplication.imageLoader.displayImage(bar.getLogo(), holder.imgBar);
		
		return vi;
	}

	public Bar[] getBars() {
		return bars;
	}

	public void setBars(Bar[] bars) {
		this.bars = bars;
	}

	
}
