package com.doisdoissete.chopps.view.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.devsmart.android.ui.HorizontalListView;
import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.ChoppTypes;
import com.doisdoissete.chopps.model.PackageBar;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderBarPackages;
import com.google.analytics.tracking.android.Log;

public class PackageBarAdapter extends BaseAdapter {

	private Context mContext;
	private List<PackageBar> pkgBars;
	private Map<Integer, ChoppTypes> choppTypes;
	
	public PackageBarAdapter(Context context, List<PackageBar> pkgBars, Map<Integer, ChoppTypes> choppTypes) {
		this.mContext = context;
		this.pkgBars = pkgBars;
		this.choppTypes = choppTypes;
	}
	
	public static void setListViewSize(HorizontalListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            //do nothing return null
            return;
        }
        //set listAdapter in loop for getting final size
        int totalHeight = 0;
        if (myListAdapter.getCount() > 0) {
        	View listItem = myListAdapter.getView(0, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //setting listview item in adapter
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight;// + (myListView.getDividerHeight() * (myListAdapter.getCount() - 1));
        myListView.setLayoutParams(params);
    }

	@Override
	public int getCount() {
		if (pkgBars == null) {
			return 0;
		}
		return pkgBars.size();
	}

	@Override
	public PackageBar getItem(int position) {
		return pkgBars.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView; // trying to reuse a recycled view

		ViewHolderBarPackages holder = null;

		if (vi == null) {
			vi = LayoutInflater.from(parent.getContext()).inflate(R.layout.bar_row_packges_view, parent, false);
			holder = new ViewHolderBarPackages();
			holder.txtMark = (TextView)vi.findViewById(R.id.txt_chopp_mark);
			holder.txtQtdeType = (TextView)vi.findViewById(R.id.txt_qtde_type_choop);
			holder.txtValor1 = (TextView)vi.findViewById(R.id.txt_preco_1);
			holder.txtValor2 = (TextView)vi.findViewById(R.id.txt_preco_2);
			holder.viewSepTop = vi.findViewById(R.id.view_sep_top);
			holder.viewSepBottom = vi.findViewById(R.id.view_sep_bottom);
			holder.btComprarPacote = (Button)vi.findViewById(R.id.bt_comprar_pacote);
			holder.layoutPreco = (LinearLayout)vi.findViewById(R.id.layout_preco);
			holder.imgChopps = (ImageView)vi.findViewById(R.id.img_qtde_type_choop);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderBarPackages) vi.getTag();
		}

		PackageBar pkgBar = getItem(position);
		
		holder.txtMark.setText(mContext.getString(R.string.txt_chopp_marca, pkgBar.getMark()));
		holder.txtQtdeType.setText(mContext.getString(R.string.txt_qtde_type, pkgBar.getQtde(), this.choppTypes.get(pkgBar.getType()).getDesc()));
		holder.txtValor1.setText(mContext.getString(R.string.txt_preco, pkgBar.getOriginalValueStr()));
		holder.txtValor2.setText(mContext.getString(R.string.txt_preco, pkgBar.getValueStr()));
		setChoppsImg(holder.imgChopps, pkgBar.getQtde(), pkgBar.getType());
		
		resizeViewTopEsq(holder.viewSepTop, holder.txtQtdeType);
		resizeViewBottom(vi, holder.viewSepBottom, holder.layoutPreco, holder.btComprarPacote);
		
		return vi;
	}
	
	private void setChoppsImg(ImageView img, Integer qtde, Integer type) {
		img.setImageResource(ChoppTypes.getIconPacotesBares(type, qtde));
	}
	
	private void resizeViewTopEsq(View viewSepTop, TextView txtRef) {
		
		txtRef.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		LayoutParams params = (LayoutParams)viewSepTop.getLayoutParams();
		params.topMargin = txtRef.getMeasuredHeight()/2;
		viewSepTop.setLayoutParams(params);
		
	}
	
	private void resizeViewBottom(final View vi, final View viewSepBottom, final View layoutPreco, final Button btRef) {
		
		layoutPreco.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				btRef.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
				LayoutParams params = (LayoutParams)viewSepBottom.getLayoutParams();
				Log.e(" params.getWidth1: " + params.width);
				params.topMargin = 200;
				params.width = btRef.getLeft() + btRef.getMeasuredWidth() /2;
				//Log.e("btRef.getLeft() = " + btRef.getMeasuredWidth() + " - params.width: " + params.width);
				viewSepBottom.setLayoutParams(params);
				viewSepBottom.setMinimumWidth(300);
				viewSepBottom.requestLayout();
				viewSepBottom.invalidate();
				
				//viewSepBottom.
				Log.e(" params.getWidth2: " + viewSepBottom.getWidth() + " width: " + viewSepBottom.getLayoutParams().width);
				vi.requestLayout();
				
			}
			
		}, 5000);
		
		
	}
	
}
