package com.doisdoissete.chopps.view.adapter;

import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.chopps.R;
import com.doisdoissete.chopps.model.ChoppTypes;
import com.doisdoissete.chopps.model.PackageUsers;
import com.doisdoissete.chopps.view.adapter.holder.ViewHolderChoosePackageToDrink;

public class ChoosePackageToDrinkAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private Context mContext;
	private PackageUsers[] packageUsers;
	private Map<Integer, ChoppTypes> choppTypes;
	
	public ChoosePackageToDrinkAdapter(Context context, PackageUsers[] packageUsers, Map<Integer, ChoppTypes> choppTypes) {
		this.inflater = LayoutInflater.from(context);
		this.mContext = context;
		this.packageUsers = packageUsers;
		this.choppTypes = choppTypes;
	}

	@Override
	public int getCount() {
		if (packageUsers == null) {
			return 0;
		}
		return packageUsers.length;
	}
	
	@Override
	public PackageUsers getItem(int position) {
		return packageUsers[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		
		ViewHolderChoosePackageToDrink holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.drink_chopp_row, parent, false);
			holder = new ViewHolderChoosePackageToDrink();
			holder.imgChoppType = (ImageView)vi.findViewById(R.id.img_chopp_type);
			holder.txtMark = (TextView)vi.findViewById(R.id.txt_mark);
			holder.txtQtdeType = (TextView) vi.findViewById(R.id.txt_qtde_type);
			holder.txtVencimento = (TextView) vi.findViewById(R.id.txt_vencimento);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderChoosePackageToDrink) vi.getTag();
		}

		PackageUsers pkgUser = packageUsers[position];
		holder.txtMark.setText(pkgUser.getPackageBar().getMark());
		ChoppTypes choopType = choppTypes.get(pkgUser.getPackageBar().getType());
		holder.txtQtdeType.setText(mContext.getString(R.string.txt_qtde_type, pkgUser.getPackageBar().getQtde(), choopType.getDesc()));
		int img = ChoppTypes.getIconEscolher(pkgUser.getPackageBar().getType());
		holder.imgChoppType.setImageResource(img);
		
		if (pkgUser.getDtExpire() != null) {
			holder.txtVencimento.setText(mContext.getString(R.string.txt_vencimento_dt, pkgUser.getDtExprireStr()));
		}
		return vi;
	}
}
