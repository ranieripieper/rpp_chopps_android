package com.doisdoissete.chopps.model;

import java.io.Serializable;

public class PackageBar extends BaseModel implements Serializable {

	private static final long serialVersionUID = 9008369163611973224L;

	public static String FIELD_VALUE = "value";
	public static String FIELD_TYPE = "type";
	
	private String id;
	private String mark;
	private Integer qtde;
	private Integer type;
	private Float value;
	private Float originalValue;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public Integer getQtde() {
		return qtde;
	}
	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Float getValue() {
		return value;
	}
	public void setValue(Float value) {
		this.value = value;
	}
	public Float getOriginalValue() {
		return originalValue;
	}
	public void setOriginalValue(Float originalValue) {
		this.originalValue = originalValue;
	}	
	public String getOriginalValueStr() {
		if (this.originalValue != null) {
			return String.format("%.2f", originalValue);
		}
		return "";
	}
	public String getValueStr() {
		if (this.value != null) {
			return String.format("%.2f", value);
		}
		return "";
	}
}
