package com.doisdoissete.chopps.model;

import java.io.Serializable;

public class ViewBarResult extends BaseModel implements Serializable {

	private static final long serialVersionUID = -7876327411804067861L;

	private Bar bar;

	public Bar getBar() {
		return bar;
	}

	public void setBar(Bar bar) {
		this.bar = bar;
	}
}
