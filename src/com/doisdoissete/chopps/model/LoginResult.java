package com.doisdoissete.chopps.model;

import com.google.gson.annotations.SerializedName;

public class LoginResult extends BaseModel {

	@SerializedName("user")
	private User userModel;
	private int nrBars;
	private boolean userHasPkg;
	private Bar bar;
	private ChoppTypes[] choppTypes;
	
	public User getUserModel() {
		return userModel;
	}

	public void setUserModel(User userModel) {
		this.userModel = userModel;
	}

	public int getNrBars() {
		return nrBars;
	}

	public void setNrBars(int nrBars) {
		this.nrBars = nrBars;
	}

	public boolean isUserHasPkg() {
		return userHasPkg;
	}

	public void setUserHasPkg(boolean userHasPkg) {
		this.userHasPkg = userHasPkg;
	}

	public ChoppTypes[] getChoppTypes() {
		return choppTypes;
	}

	public void setChoppTypes(ChoppTypes[] choppTypes) {
		this.choppTypes = choppTypes;
	}

	public Bar getBar() {
		return bar;
	}

	public void setBar(Bar bar) {
		this.bar = bar;
	}

	

	
}
