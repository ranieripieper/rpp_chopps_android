package com.doisdoissete.chopps.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User extends BaseModel implements Serializable {

	private static final long serialVersionUID = -7241478170800806098L;

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy '�s' HH'h'mm");
	public static String FIELD_EMAIL = "user[email]"; 
	public static String FIELD_NAME = "user[name]"; 
	public static String FIELD_PHONE = "user[phone]"; 
	public static String FIELD_PASSWORD = "user[password]"; 
	public static String FIELD_PASSWORD_CONF = "user[password_confirmation]"; 
	public static String FIELD_PHOTO = "user[photo]"; 
	public static String FIELD_USER_TOKEN = "User-Token"; 
	
	private String id;
	private String name;
	private String telefone;
	private String password;
	private String email;
	private String authenticationToken;
	private String photo;
	private String lastChoppBar;
	private Date lastChoppDt;
	private PackageUsers[] packageUsers;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAuthenticationToken() {
		return authenticationToken;
	}
	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getLastChoppBar() {
		return lastChoppBar;
	}
	public void setLastChoppBar(String lastChoppBar) {
		this.lastChoppBar = lastChoppBar;
	}
	public Date getLastChoppDt() {
		return lastChoppDt;
	}
	public void setLastChoppDt(Date lastChoppDt) {
		this.lastChoppDt = lastChoppDt;
	}
	public PackageUsers[] getPackageUsers() {
		return packageUsers;
	}
	public void setPackageUsers(PackageUsers[] packageUsers) {
		this.packageUsers = packageUsers;
	}
	
	public String getLastChoppDtStr() {
		if (this.lastChoppDt != null) {
			return sdf.format(this.lastChoppDt);
		}
		return "";
	}
	
}
