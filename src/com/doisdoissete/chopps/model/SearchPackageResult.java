package com.doisdoissete.chopps.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class SearchPackageResult extends BaseModel implements Serializable {

	private static final long serialVersionUID = 6747412312940292852L;
	
	@SerializedName("data")
	private Bar[] bars;

	public Bar[] getBars() {
		return bars;
	}

	public void setBars(Bar[] bar) {
		this.bars = bar;
	}
	
	

}
