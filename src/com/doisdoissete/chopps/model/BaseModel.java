package com.doisdoissete.chopps.model;


public class BaseModel {

	public static String FIELD_ID = "id";
	public static String FIELD_LAT = "lat"; 
	public static String FIELD_LNG = "lng"; 
	
	private boolean success;
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	
}
