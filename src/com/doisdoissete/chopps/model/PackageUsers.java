package com.doisdoissete.chopps.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class PackageUsers extends BaseModel implements Serializable {

	private static final long serialVersionUID = -9212693052720274991L;

	private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	
	private String id;
	private Date dtExpire;
	private Integer qtdeCons;
	private PackageBar packageBars;
	
	@SerializedName("package")
	private PackageBar packageBar;
	private Bar bar;
	
	public Date getDtExpire() {
		return dtExpire;
	}
	public void setDtExpire(Date dtExpire) {
		this.dtExpire = dtExpire;
	}
	public Integer getQtdeCons() {
		return qtdeCons;
	}
	public void setQtdeCons(Integer qtdeCons) {
		this.qtdeCons = qtdeCons;
	}
	public PackageBar getPackageBars() {
		return packageBars;
	}
	public void setPackageBars(PackageBar packageBars) {
		this.packageBars = packageBars;
	}
	public Bar getBar() {
		return bar;
	}
	public void setBar(Bar bar) {
		this.bar = bar;
	}
	
	public String getDtExprireStr() {
		if (dtExpire != null) {
			return sdf.format(dtExpire);
		}
		return "";
	}
	public PackageBar getPackageBar() {
		return packageBar;
	}
	public void setPackageBar(PackageBar packageBar) {
		this.packageBar = packageBar;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
