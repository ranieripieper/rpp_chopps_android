package com.doisdoissete.chopps.model;

import java.io.Serializable;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.chopps.R;

public class ChoppTypes implements Serializable {

	private static final long serialVersionUID = -7833932184047793142L;
	
	private Integer id;
	private String desc;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public static String toJson(ChoppTypes obj) {
		return Util.getGson().toJson(obj);
	}
	
	public static ChoppTypes fromJson(String json) {
		return Util.getGson().fromJson(json, ChoppTypes.class);
	}
	
	public static String toJson(ChoppTypes[] obj) {
		return Util.getGson().toJson(obj);
	}
	
	public static int getIconEscolher(int id) {
		if (id == 1) {
			return R.drawable.img_escolher_claro;
		} else {
			return R.drawable.img_escolher_escuro;
		}
	}
	
	public static int getIconPacotesBares(int id, int qtde) {

		if (id == 1) {
			if (qtde < 20) {
				 return R.drawable.img_pacotes_bares_10_chopps_claros;
			} else if (qtde < 30) {
				return R.drawable.img_pacotes_bares_20_chopps_claros;
			} else {
				return R.drawable.img_pacotes_bares_30_chopps_claros;
			}			
		} else {
			if (qtde < 20) {
				return R.drawable.img_pacotes_bares_10_chopps_escuros;
			} else if (qtde < 30) {
				return R.drawable.img_pacotes_bares_20_chopps_escuros;
			} else {
				return R.drawable.img_pacotes_bares_30_chopps_escuros;
			}			
		}
	}
}
