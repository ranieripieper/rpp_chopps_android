package com.doisdoissete.chopps.model;

import java.io.Serializable;
import java.util.Arrays;

public class InfoException implements Serializable {

	private static final long serialVersionUID = -8516344778610940188L;
	
	private String[] password;
	private String[] passwordConfirmation;
	private String[] email;
	private String[] name;
	private String[] phone;
	public String[] getPassword() {
		return password;
	}
	public void setPassword(String[] password) {
		this.password = password;
	}
	public String[] getPasswordConfirmation() {
		return passwordConfirmation;
	}
	public void setPasswordConfirmation(String[] passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
	public String[] getEmail() {
		return email;
	}
	public void setEmail(String[] email) {
		this.email = email;
	}
	public String[] getName() {
		return name;
	}
	public void setName(String[] name) {
		this.name = name;
	}
	public String[] getPhone() {
		return phone;
	}
	public void setPhone(String[] phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		String res =  
				fieldString(name) + 
				fieldString(email) + 
				fieldString(phone) + 
				fieldString(password) + 
				fieldString(passwordConfirmation);
		
		if (res.lastIndexOf("\n") > 0) {
			res = res.substring(0, res.lastIndexOf("\n"));
		}
		return res;
	}
	
	private String fieldString(String[] field) {
		String result = "";
		if (field != null) {
			for (String f : field) {
				result += f + "\n";
			}
		}
		
		return result;
	}
	
	
}
