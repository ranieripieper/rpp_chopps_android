package com.doisdoissete.chopps.model;

import java.io.Serializable;

public class ExceptionResponse extends BaseModel implements Serializable {

	private static final long serialVersionUID = 8135952884082510779L;
	

	public InfoException info;


	public InfoException getInfo() {
		return info;
	}


	public void setInfo(InfoException info) {
		this.info = info;
	}


	@Override
	public String toString() {
		return info.toString();
	}


	
}
