package com.doisdoissete.chopps.model;

import java.io.Serializable;

public class Address implements Serializable {

	private static final long serialVersionUID = 4657326585829721608L;

	private String address;
	private String street;
	private Double[] coordinates;
	private String number;
	private String neighborhood;
	private String state;
	private String city;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public Double[] getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(Double[] coordinates) {
		this.coordinates = coordinates;
	}

	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getNeighborhood() {
		return neighborhood;
	}
	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	public String getAddressToView() {
		return String.format("%s, %s\n%s, %s,%s", this.street, this.number, neighborhood, this.city, this.state);
	}
	
}
