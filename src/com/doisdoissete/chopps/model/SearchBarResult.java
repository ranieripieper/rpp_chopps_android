package com.doisdoissete.chopps.model;

import java.io.Serializable;

public class SearchBarResult extends BaseModel implements Serializable {

	private static final long serialVersionUID = 6747412312940292852L;

	private Bar[] bars;

	public Bar[] getBars() {
		return bars;
	}

	public void setBars(Bar[] bars) {
		this.bars = bars;
	}

}
