package com.doisdoissete.chopps.model;

import java.io.Serializable;

public class PackageUserResult extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1062411621720456342L;
	private PackageUsers[] packages;

	public PackageUsers[] getPackages() {
		return packages;
	}

	public void setPackages(PackageUsers[] packages) {
		this.packages = packages;
	}
}
