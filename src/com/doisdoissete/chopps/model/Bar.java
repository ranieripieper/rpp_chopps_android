package com.doisdoissete.chopps.model;

import java.io.Serializable;

public class Bar extends BaseModel implements Serializable {

	private static final long serialVersionUID = -2448914264538617951L;
	
	public static final String FIELD_SEARCH = "search";
	public static final String FILTER_BAR_ID = "bar_id";
	
	private String id;
	private String name;
	private String phone;
	private Double geoNearDistance;
	private String logo;
	private Address address;
	private PackageBar[] packageBars;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Double getGeoNearDistance() {
		return geoNearDistance;
	}
	
	public String getGeoNearDistanceStr() {
		if (geoNearDistance != null) {
			if (geoNearDistance < 1 ) {
				return String.format("%.0fm", geoNearDistance*1000);
			} else {
				return String.format("%.2fKm", geoNearDistance);
			}
		}
		return "";
		
	}
	public void setGeoNearDistance(Double geoNearDistance) {
		this.geoNearDistance = geoNearDistance;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public PackageBar[] getPackageBars() {
		return packageBars;
	}
	public void setPackageBars(PackageBar[] packageBars) {
		this.packageBars = packageBars;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

}
