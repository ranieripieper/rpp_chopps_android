package com.doisdoissete.chopps.model;

import java.lang.reflect.Type;

import com.doisdoissete.android.util.ddsutil.Util;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class UserModelDeserializer implements JsonDeserializer<LoginResult> {

	@Override
	public LoginResult deserialize(final JsonElement json, final Type typeOfT,
			final JsonDeserializationContext context) throws JsonParseException {
		
		final JsonObject jsonObject = json.getAsJsonObject();
		LoginResult lgResult = null;
		Gson gs = Util.getGson();
		if (jsonObject != null) {
			lgResult = gs.fromJson(jsonObject, LoginResult.class);
			/*
			if (lgResult != null && lgResult.getData() != null
					 && lgResult.getData().get("user") != null) {
				User userModel = gs.fromJson(lgResult.getData().get("user"), User.class);
				lgResult.setUserModel(userModel);
			}
			*/
		}	

		return lgResult;
	}

}
