package com.doisdoissete.chopps;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.FixedHeaderListView;
import com.doisdoissete.chopps.model.LoginResult;
import com.doisdoissete.chopps.model.User;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.util.Preferences;
import com.doisdoissete.chopps.view.activity.LoginActivity;
import com.doisdoissete.chopps.view.activity.SearchBarActivity;
import com.doisdoissete.chopps.view.activity.SearchPackageActivity;
import com.doisdoissete.chopps.view.activity.SignUpActivity;
import com.doisdoissete.chopps.view.activity.base.BaseActivity;
import com.doisdoissete.chopps.view.adapter.HomeAdapter;
import com.google.gson.Gson;


public class MainActivity extends BaseActivity {

	public static String INTENT_PARAM_HOME = "INTENT_PARAM_HOME";
	
	private static int SPLASH_TIME_OUT = 500;
	private static int SMOOTH_DURATION = 3000;
	
	private boolean showHome = false;
	
	private FixedHeaderListView mListView;
	private LoginResult loginResult;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        mListView = (FixedHeaderListView)findViewById(R.id.fixedHeaderListView);

        if (Preferences.getUserToken(MainActivity.this) != null) {
        	showHome = true;
        } else {
        	showHome = false;
        }
        
        init(true);
        
/*        Intent it = new Intent(MainActivity.this, ViewBarActivity.class);
		it.putExtra(ViewBarActivity.PARAM_BAR_ID, "545a6f2752616e2c33070000");
		
		startActivity(it);*/
		
       // buyPackageClick(null);
    }
    
    private void init(boolean wait) {
		if (showHome) {
			showHome();
		} else {
			showLoginSingUp();
			Preferences.setUserToken(this, null);
		}
		
		if (wait) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					animate();
				}
			}, SPLASH_TIME_OUT);
		} else {
			animate();
		}
		
		mListView.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
		
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Toast.makeText(MainActivity.this, "click", Toast.LENGTH_SHORT).show();
				
			}
		});
		
    }
    
    private void animate() {
    	if (mListView.getChildCount() > 0) { 	
	    	if (showHome && loginResult != null) {
	    		ScreenUtil.smotthListView(mListView.getListView(), 3);
			} else if (!showHome && mListView.getChildCount() >= 1) {
				ScreenUtil.smotthListView(mListView.getListView(), 1);
			}
    	}
    }
    
	private void showHome() {
		if (loginResult != null) {
			if (loginResult.getUserModel() != null && !TextUtils.isEmpty(loginResult.getUserModel().getAuthenticationToken())) {
				Preferences.setUserToken(MainActivity.this, loginResult.getUserModel().getAuthenticationToken());
			}
			if (loginResult.getUserModel() != null && loginResult.getChoppTypes() != null && loginResult.getChoppTypes().length > 0) {
				Preferences.setChoppTypes(MainActivity.this, loginResult.getChoppTypes());				
			}
			dismissWaitDialog();
			HomeAdapter adapter = new HomeAdapter((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE), this, showHome, loginResult);
			mListView.setAdapter(adapter);
		} else {
			callGetUserInfo();
		}
	}
	
	private void showLoginSingUp() {
		dismissWaitDialog();
		HomeAdapter adapter = new HomeAdapter((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE), this, showHome);
		mListView.setAdapter(adapter);
		
		mListView.setScrollContainer(false);
		mListView.setOnTouchListener(new View.OnTouchListener() {

		    public boolean onTouch(View v, MotionEvent event) {
		        return (event.getAction() == MotionEvent.ACTION_MOVE);
		    }
		});
	}
	
	/**
	 * A��o no clique do bot�o login
	 * @param v
	 */
	public void loginClick(View v) {
		Intent i = new Intent(this, LoginActivity.class);
        startActivityForResult(i, RESULT_LOGIN_ACTIVITY);
	}
	
	private int RESULT_LOGIN_ACTIVITY = 1;
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    if (requestCode == RESULT_LOGIN_ACTIVITY && resultCode == RESULT_OK
	            && null != data) {
	    	
	    	mListView.getListView().smoothScrollBy(-500, SMOOTH_DURATION);
	    	
	    	 new Handler().postDelayed(new Runnable() {

	 			@Override
	 			public void run() {
	 				showHomePosLogin();
	 			}
	 		}, SMOOTH_DURATION);
	    	 
	    	String parse = data.getExtras().getString(INTENT_PARAM_HOME);
		    	
		    Gson gson = Util.getGson(null, null, null);
		    
		    loginResult = gson.fromJson(parse, LoginResult.class);
	    }
	}
	
	private void showHomePosLogin() {
		showHome = true;
		init(false);
	}
	
	/**
	 * A��o no clique do bot�o SignUp
	 * @param v
	 */
	public void signUpClick(View v) {
		Intent i = new Intent(this, SignUpActivity.class);
        startActivityForResult(i, RESULT_LOGIN_ACTIVITY);
	}
	
	
	private void callGetUserInfo() {
		ObserverAsyncTask<LoginResult> observer = new ObserverAsyncTask<LoginResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(LoginResult result) {
				dismissWaitDialog();
				loginResult = result;
				if (result == null || !result.isSuccess()) {
					showHome = false;
				} else {
					showHome = true;
				}
				init(false);
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				showHome = false;
				init(false);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(User.FIELD_USER_TOKEN, Preferences.getUserToken(MainActivity.this));
		
		Location loc = GpsUtil.getLocation(MainActivity.this);
		if (loc != null) {
			params.add(User.FIELD_LAT, String.valueOf(loc.getLatitude()));
			params.add(User.FIELD_LNG, String.valueOf(loc.getLongitude()));
		}
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Constants.GET_USER_INFO_PATH)
			.setObserverAsyncTask(observer)
			.setHttpMethod(HttpMethod.GET)
			.setNeedConnection(true)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(this, LoginResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	
	/**
	 * A��o no clique do bot�o Procurar Bar
	 * @param v
	 */
	public void searchBarClick(View v) {
		startActivity(new Intent(this, SearchBarActivity.class));
	}
	
	/**
	 * A��o no clique do bot�o Comprar Pacote
	 * @param v
	 */
	public void buyPackageClick(View v) {
		startActivity(new Intent(this, SearchPackageActivity.class));
	}
	
	@Override
	protected boolean isHideActionBar() {
		return true;
	}
	
	protected boolean customOpenClose() {
		return false;
	}
	
}