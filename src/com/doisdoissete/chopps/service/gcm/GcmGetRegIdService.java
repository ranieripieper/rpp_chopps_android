package com.doisdoissete.chopps.service.gcm;

import java.io.IOException;
import java.net.ConnectException;

import android.content.Context;
import android.util.Log;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.chopps.util.Constants;
import com.doisdoissete.chopps.util.Preferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmGetRegIdService extends BaseService {

	private static GoogleCloudMessaging gcm;
	
	public GcmGetRegIdService(ServiceBuilder builder) {
		super(builder);
		
	}
	
	@Override
	public void refreshServiceBuilder(ServiceBuilder builder) {
		super.refreshServiceBuilder(builder);
		gcm = GoogleCloudMessaging.getInstance(builder.getmContext());
	}
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException {
		String regId = null;
		try {
			if (checkPlayServices(ctx)) {
				regId = gcm.register(Constants.SENDER_ID);
				Preferences.setRegId(ctx, regId);
			}
			
		} catch (IOException e) {
		}
		
		return regId;
	}
	
	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices(Context ctx) {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(ctx);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	        	Log.i(GcmGetRegIdService.class.toString(), "This device isUserRecoverableError.");
	        } else {
	            Log.i(GcmGetRegIdService.class.toString(), "This device is not supported.");
	        }
	        return false;
	    }
	    return true;
	}
	
}
